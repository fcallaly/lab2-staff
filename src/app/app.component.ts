import { Component } from '@angular/core';
import { EmployeeService } from './employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'ng-lab2';
  employees: any;

  constructor(private employeeService: EmployeeService) { }

  ngOnInit(): void {
    this.employeeService.findAll().subscribe(
      responseData => {
        console.log("Recieved Response Data:");
        console.log(responseData);
        this.employees = responseData;
      },
      error => {
        console.log("Error retreiving employee list");
        console.log(error);
      }
    );
  }
}
